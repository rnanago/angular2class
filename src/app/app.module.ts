import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import {AngularFireModule} from 'angularfire2';
import {UsersService } from './users/users.service';
import {ProductsService } from './products/products.service';
import {InvoicesService } from './invoices/invoices.service';


import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { ProductComponent } from './product/product.component';
import { ProductsComponent } from './products/products.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceComponent } from './invoice/invoice.component';



export const firebaseConfig = {
    apiKey: "AIzaSyAH-38TevbVNsv7z15fQQSjUev-c9qT2Cc",
    authDomain: "angular2-86c97.firebaseapp.com",
    databaseURL: "https://angular2-86c97.firebaseio.com",
    storageBucket: "angular2-86c97.appspot.com",
    messagingSenderId: "1054635124298"
  }

const appRoutes:Routes = [
 
  {path:'users',component:UsersComponent},
  {path:'posts',component:PostsComponent},
  {path:'products',component:ProductsComponent},
  {path:'invoices',component:InvoicesComponent},
  {path:'invoice',component:InvoiceComponent},
  {path:'invoice-form',component:InvoiceFormComponent},
  {path:'',component:InvoiceFormComponent},//כשלא מצוין ראוט הדיפולטיבי הוא יוזר
 {path:'**',component:PageNotFoundComponent},//כשיש עמוד לא קיים
]



@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent,
    ProductComponent,
    ProductsComponent,
    InvoiceFormComponent,
    InvoicesComponent,
    InvoiceComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
   
  ],
  providers: [UsersService,ProductsService, InvoicesService ], 
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Invoice} from '../invoice/invoice'
 

@Component({
  selector: 'jce-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.css']
})
export class InvoiceFormComponent implements OnInit {


  
  currentInvoice;
  
  select(invoice){
 		this.currentInvoice = invoice; 
    console.log(	this.currentInvoice);
  }

  @Output() invoiceAddedEvent = new EventEmitter<Invoice>();
  invoice:Invoice = {
    name: '',
    amount: ''
  };

  constructor() { 
  }

 onSubmit(form:NgForm){
    console.log(form);
    this.invoiceAddedEvent.emit(this.invoice);
    this.invoice = {

       name: '',
       amount: ''
    }
  }

  ngOnInit() {
  }

}

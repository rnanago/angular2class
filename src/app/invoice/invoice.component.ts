import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import {Invoice} from './invoice';

@Component({
  selector: 'jce-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
  inputs:['invoice']
})
export class InvoiceComponent implements OnInit {
/*
   @Intput() setInvoice = new EventEmitter<invoice>();
 

  invoicSet(){
  this.setInvoice.emit(this.invoice);
}
*/
   invoice:Invoice;

  constructor() { }

  ngOnInit() {
  }

}

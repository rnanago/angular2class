import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { InvoicesService } from './invoices.service';
import {Invoice} from '../invoice/invoice'


@Component({
  selector: 'jce-invoices',
  templateUrl: './invoices.component.html',
   styles: [` .invoices li { cursor: default; }
    .invoices li:hover { background: #ecf0f1; } `]
})
export class InvoicesComponent implements OnInit {

invoices;

currentInvoice;

  isLoading = true;
/*
  @Input() invoiceAddedEvent = new EventEmitter<Invoice>();
  invoice:Invoice = {
    name: '',
    amount: ''
  };

  setInvoices(invoice){
  this._invoiceService.setInvoice(invoice);
}

*/
  select(invoice){
 		this.currentInvoice = invoice; 
    console.log(	this.currentInvoice);
  }

   addInvoice(invoice){
    this.invoices.push(invoice)
  }


  constructor(private _invoiceService:InvoicesService) { }

  ngOnInit() {
    this._invoiceService.getInvoices().subscribe(invoiceData => {this.invoices=invoiceData;this.isLoading = false});

  }
}

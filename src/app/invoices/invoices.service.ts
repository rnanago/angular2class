import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2';
import 'rxjs/add/operator/delay';

@Injectable()
export class InvoicesService {

  invoicesObservable;

getInvoices(){
this.invoicesObservable=this.af.database.list('/invoices');
return this.invoicesObservable.delay(2000);
}

/*setInvoices(invoice){
let postKey=invoice.$key;
this.af.database.object('/invoices/'+postKey);
 }*/
  constructor(private af:AngularFire) { }

}
